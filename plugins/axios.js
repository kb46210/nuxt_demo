import Axios from 'axios'
import MockAdapter from 'axios-mock-adapter'
export default function (ctx, inject) {
  const axios = Axios.create({
    baseURL: 'https://api.example.com'
  })
  // axios.defaults.baseURL = 'https://api.example.com'
  // axios.defaults.headers.common.Authorization = 'YUOR_AUTH_TOKEN'
  // axios.defaults.headers.post['Content-Type'] = 'application/x-www-form-urlencoded'

  // ctx.$axios = axios
  // inject('axios', axios)

  const mock = new MockAdapter(axios)
  const data = { users: [
    { id: 1, name: 'John Smith' }
  ] }
  mock.onPost('/users').reply((config) => {
    return new Promise((resolve, reject) => {
      setTimeout(() => resolve([200, { data }]), 0)
    })
  })
}
