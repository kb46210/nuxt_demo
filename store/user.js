export const state = () => ({
  info: { }
})
export const mutations = {
  /**
   * userInfo method
   * @param {*} state store
   * @param {*} info info
   */
  insert (state, { userinfo }) {
    state.info = userinfo
  },
  updat (state, { userinfo }) {
    for (const prop in userinfo) { state.info[prop] = userinfo[prop] }
  }
}
